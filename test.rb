module AddMethod
  def self.included(base)
    base.module_eval do
      define_method 'new_method' do
        "it's a new method"
      end
    end
  end
end

class Foo
  def self.static_method
    "it's a static method"
  end
  
  def do_include
    $stderr.puts singleton_class.ancestors.inspect
    Foo.include AddMethod
    $stderr.puts singleton_class.ancestors.inspect
  end
end
Foo.static_method
f = Foo.new
f.do_include
f.new_method

class Bar
  def do_include
    class << self
      $stderr.puts ancestors.inspect
      include AddMethod
      $stderr.puts ancestors.inspect
    end
  end
end
b = Bar.new
b.do_include
b.new_method
